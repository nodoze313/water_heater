#!/bin/ash

NOW_FILE=/tmp/NOW
ON_FILE=/root/ON
OFF_FILE=/root/OFF

debug() {
  [ -f /root/debug ] && echo "$@"
}

on() {
  gpioctl dirout-high 40
}

off() {
  gpioctl dirout-low 40
}

hour_cmd() {
  touch --date "@$(date +%s)" $ON_FILE
  touch --date "@$(( $(date +%s) + 3600 ))" $OFF_FILE
}

state() {
  gpioctl get 40 | grep -q HIGH && echo "ON" || echo "OFF"
}

gw() {
  ip route sho | grep default | awk '{print $3}'
}

connection_test() {
  st=$(state)
  if [[ "$st" == "OFF" ]]; then
    ping -c 3 $(gw) || reboot
  else
    echo "Waiting till off"
  fi
}

ensure() {
  [ -f $ON_FILE ] || { off; return 0; }
  [ -f $OFF_FILE ] || { off; return 0; }
  touch $NOW_FILE
  if [[ $OFF_FILE -ot  $NOW_FILE ]]; then
    off
  else
    if [[ $NOW_FILE -nt $ON_FILE ]]; then
      on
    else
      off
    fi
  fi
  connection_test
}

$1


